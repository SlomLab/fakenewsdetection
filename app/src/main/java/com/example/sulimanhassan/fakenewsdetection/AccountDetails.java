package com.example.sulimanhassan.fakenewsdetection;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class AccountDetails extends Fragment {

    private TextView genderTV, ageTV, locationTV, domainTv, emailTV;
    private Button signOutBtn;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private FirebaseAuth mAuth;
    private ProgressBar progressBar;
    private FirebaseUser currentUser;

    View view;

    public AccountDetails() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_account_details, container, false);

        getActivity().setTitle("Account");
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Users");
        currentUser = mAuth.getCurrentUser();

        genderTV = (TextView) view.findViewById(R.id.genderTextView);
        ageTV = (TextView) view.findViewById(R.id.ageTextView);
        locationTV = (TextView) view.findViewById(R.id.locationTextView);
        emailTV = (TextView) view.findViewById(R.id.emailTextView);
        emailTV.setText(currentUser.getEmail());
        domainTv = (TextView) view.findViewById(R.id.domainTextView);

        signOutBtn = (Button) view.findViewById(R.id.signOutBtn);
        signOutBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mAuth.signOut();
                MainActivity.u = new User();

                getActivity().finish();
                startActivity(getActivity().getIntent().putExtra("ssdf", "sdf").addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
            }
        });

        fetchData();
        return view;
    }

    private void fetchData() {
        try {

            if (MainActivity.u.getDomain().equals("")) {
                myRef.child(currentUser.getUid() + "/info").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        progressBar.setVisibility(View.VISIBLE);
                        for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                            switch (childSnapshot.getKey().toString()) {
                                case "age":
                                    ageTV.setText(childSnapshot.getValue() + "");
                                    Log.d(TAG, "onDataChange: " + childSnapshot.getValue());
                                    MainActivity.u.setAge(childSnapshot.getValue() + "");
                                    break;
                                case "experience":
                                    domainTv.setText(childSnapshot.getValue().toString());
                                    MainActivity.u.setDomain(childSnapshot.getValue().toString());
                                    Log.d(TAG, "106: " + childSnapshot.getValue());
                                    break;
                                case "gender":
                                    genderTV.setText(childSnapshot.getValue().toString());
                                    MainActivity.u.setGender(childSnapshot.getValue().toString());
                                    Log.d(TAG, "111: " + childSnapshot.getValue());
                                    break;
                                case "location":
                                    locationTV.setText(childSnapshot.getValue().toString());
                                    MainActivity.u.setLocation(childSnapshot.getValue().toString());
                                    break;
                                default:
                                    break;
                            }
                            Log.d(TAG, "onDataChange: " + childSnapshot.getKey() + " " + childSnapshot.getValue());
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d(TAG, "onDataChange: ");
                    }
                });
            } else {
                domainTv.setText(MainActivity.u.getDomain());
                locationTV.setText(MainActivity.u.getLocation());
                genderTV.setText(MainActivity.u.getGender());
                ageTV.setText(MainActivity.u.getAge());
            }
        } catch (Exception e) {
            Log.d(TAG, "Exceprion e: " + e.getMessage());
            myRef.child(currentUser.getUid() + "/info").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    progressBar.setVisibility(View.VISIBLE);
                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        switch (childSnapshot.getKey().toString()) {
                            case "age":
                                ageTV.setText(childSnapshot.getValue().toString());
                                Log.d(TAG, "onDataChange: " + childSnapshot.getValue());
                                MainActivity.u.setAge(childSnapshot.getValue() + "");
                                break;
                            case "domain":
                                domainTv.setText(childSnapshot.getValue().toString());
                                MainActivity.u.setDomain(childSnapshot.getValue().toString());
                                Log.d(TAG, "106: " + childSnapshot.getValue());
                                break;
                            case "gender":
                                genderTV.setText(childSnapshot.getValue().toString());
                                MainActivity.u.setGender(childSnapshot.getValue().toString());
                                Log.d(TAG, "111: " + childSnapshot.getValue());
                                break;
                            case "location":
                                locationTV.setText(childSnapshot.getValue().toString());
                                MainActivity.u.setLocation(childSnapshot.getValue().toString());
                                break;
                            default:
                                break;
                        }
                        Log.d(TAG, "onDataChange: " + childSnapshot.getKey() + " " + childSnapshot.getValue());
                    }
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d(TAG, "onDataChange: ");
                }
            });
        }
    }


}

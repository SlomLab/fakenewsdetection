package com.example.sulimanhassan.fakenewsdetection;


import android.app.Activity;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.TimerTask;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {


    private ProgressBar progressBar;
    private FirebaseAuth mAuth;
    private EditText EditTextEmail;
    private EditText EditTextpassword;
    private Button signInBtn;
    private TextView signUpBtn;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference mRef;
    //    private String email;
//    private String password;
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id = view.getId();
            switch (id) {
                case R.id.signInBtn:
                    String email = EditTextEmail.getText().toString();
                    String password = EditTextpassword.getText().toString();
                    signInWithEmailAndPassword(email, password);
                    break;
                case R.id.signUpBtn:
                    createUserWithEmailAndPassword();
                    break;
                default:
                    break;
            }
        }

    };


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        getActivity().setTitle("Sign In");
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            goToAccountDetails();
        }

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        EditTextEmail = (EditText) view.findViewById(R.id.email);
        EditTextEmail.requestFocus();
        EditTextpassword = (EditText) view.findViewById(R.id.password);
        signInBtn = (Button) view.findViewById(R.id.signInBtn);
        signUpBtn = (TextView) view.findViewById(R.id.signUpBtn);

        signUpBtn.setOnClickListener(mOnClickListener);
        signInBtn.setOnClickListener(mOnClickListener);

        // Inflate the layout for this fragment
        return view;
    }


    private void signInWithEmailAndPassword(String email, String password) {
        progressBar.setVisibility(View.VISIBLE);
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener((Activity) getContext(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            progressBar.setVisibility(View.GONE);
                            FirebaseUser user = mAuth.getCurrentUser();
                            Log.d(TAG, "signInWithEmail:success" + user.getEmail());

                            goToAccountDetails();
//
                        } else {
                            progressBar.setVisibility(View.GONE);
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(getActivity(), "check email or password.",
                                    Toast.LENGTH_LONG).show();

                        }

                    }
                });
    }

    private void goToAccountDetails() {
        AccountDetails accountDetails = new AccountDetails();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_frame, accountDetails, "findThisFragment")
                .addToBackStack(null)
                .commit();
    }


    private void createUserWithEmailAndPassword() {

        signUpFragment signUpFragment = new signUpFragment();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_frame, signUpFragment, "findThisFragment")
                .addToBackStack(null)
                .commit();
    }

}




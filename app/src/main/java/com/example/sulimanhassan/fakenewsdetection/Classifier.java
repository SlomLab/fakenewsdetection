package com.example.sulimanhassan.fakenewsdetection;


import android.content.Context;
import android.content.res.AssetManager;

import java.util.ArrayList;

import weka.classifiers.bayes.NaiveBayes;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SelectedTag;
import weka.core.Utils;
import weka.core.converters.ConverterUtils;
import weka.core.stemmers.IteratedLovinsStemmer;
import weka.core.tokenizers.WordTokenizer;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.ClassAssigner;
import weka.filters.unsupervised.attribute.StringToWordVector;

public class Classifier {
    public static void main(String args[]) {
//        Classifier.PredictInstance("shdjkaf ahj ksdfh ");
    }


    public static int PredictInstance(String text, Context context) { // return 1 if the Prediction is TRUE and -1 if FALSE and 0 if neither.

        double FALSE = 0, TRUE = 0;
        try {
            Instance instance = Classifier.prepareInstance(text, context);
            System.out.println("sdf");

            AssetManager assetManager = context.getAssets();

            NaiveBayes naiveBayes = (NaiveBayes) weka.core.SerializationHelper.read(assetManager.open("hope.model"));

            System.out.println("sdf");
            double[] labelArray = naiveBayes.distributionForInstance(instance);
            FALSE = labelArray[0];
            TRUE = labelArray[1];
            for (double x : labelArray
                    ) {
                System.out.println("Class: " + x);
            }


        } catch (Exception e) {
            System.out.println(e.getMessage() + " asd");
        }
//        Log.d("safsdf","Asdfkhsd"+ FALSE +" :: " + TRUE);
        if (Math.abs(FALSE - TRUE) > 0.8) {

            if (FALSE > TRUE)
                return -1;
            else
                return 1;
        } else {
            return 0;
        }
    }

    private static Instance prepareInstance(String text, Context context) {

        Attribute a1 = new Attribute("text", (FastVector) null);

        ConverterUtils.DataSource full = null;
        Instances fullData = null;
        try {
            full = new ConverterUtils.DataSource(context.getAssets().open("lastFull.arff"));
            fullData = full.getDataSet();
        } catch (Exception e) {
            e.printStackTrace();
        }


        FastVector fvClassVal = new FastVector(2);
        fvClassVal.addElement("FALSE");
        fvClassVal.addElement("TRUE");
        Attribute Class = new Attribute("class", fvClassVal);
        FastVector fvWekaAttributes = new FastVector(2);
        fvWekaAttributes.addElement(a1);
        fvWekaAttributes.addElement(Class);


        Instances i1 = new Instances("TestInstances", fvWekaAttributes, 0);

        ArrayList<Attribute> attValues = new ArrayList();
        a1.addStringValue(text);
        attValues.add(a1);
        attValues.add(Class);

        double[] instanceValue1 = new double[i1.numAttributes()];

        instanceValue1[0] = i1.attribute(0).addStringValue(text);


        i1.add(new DenseInstance(1.0, instanceValue1));
        i1.setClassIndex(1);

        StringToWordVector stringToWordVector = new StringToWordVector();
        try {
            ClassAssigner classAssigner = new ClassAssigner();
            classAssigner.setInputFormat(fullData);
            classAssigner.setClassIndex("1");
            fullData = Filter.useFilter(fullData, classAssigner);

            stringToWordVector.setOptions(Utils.splitOptions("-R first-last -W 1000 -prune-rate -1.0 -C -T -I -N 1 -stemmer weka.core.stemmers.IteratedLovinsStemmer -M 1 -tokenizer \"weka.core.tokenizers.WordTokenizer -delimiters \\\" \\\\r \\\\t\\\"\""));
//            stringToWordVector.setStemmer(new IteratedLovinsStemmer());
//            WordTokenizer wordTokenizer = new WordTokenizer();
//            wordTokenizer.setDelimiters("\\r \\t");
//            stringToWordVector.setTokenizer(wordTokenizer);
//            stringToWordVector.setNormalizeDocLength(new SelectedTag(StringToWordVector.FILTER_NORMALIZE_ALL, StringToWordVector.TAGS_FILTER));
//            stringToWordVector.setTFTransform(true);
//            stringToWordVector.setIDFTransform(true);
//            stringToWordVector.setOutputWordCounts(true);

            Instances instances = Classifier.merge(i1,fullData);

            System.out.println(instances.numInstances() + " " + instances.numAttributes() + " trying1 " + instances.firstInstance());
            stringToWordVector.setInputFormat(instances);
            Instances newInstancs = Filter.useFilter(instances, stringToWordVector);
            System.out.println(newInstancs.numInstances() + " " + newInstancs.numAttributes() + " trying2 " + newInstancs.firstInstance());

            return newInstancs.firstInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }


        System.out.println("No here");
        return null;
    }
    public static Instances merge(Instances data1, Instances data2)
            throws Exception
    {
        // Check where are the string attributes
        int asize = data1.numAttributes();
        boolean strings_pos[] = new boolean[asize];
        for(int i=0; i<asize; i++)
        {
            Attribute att = data1.attribute(i);
            strings_pos[i] = ((att.type() == Attribute.STRING) ||
                    (att.type() == Attribute.NOMINAL));
        }

        // Create a new dataset
        Instances dest = new Instances(data1);
        dest.setRelationName(data1.relationName() + "+" + data2.relationName());

        ConverterUtils.DataSource source = new ConverterUtils.DataSource(data2);
        Instances instances = source.getStructure();
        Instance instance = null;
        while (source.hasMoreElements(instances)) {
            instance = source.nextElement(instances);
            dest.add(instance);

            // Copy string attributes
            for(int i=0; i<asize; i++) {
                if(strings_pos[i]) {
                    dest.instance(dest.numInstances()-1)
                            .setValue(i,instance.stringValue(i));
                }
            }
        }

        return dest;
    }
}


//            ConverterUtils.DataSource train = new ConverterUtils.DataSource("/Users/sulimanhassan/Desktop/Graduation Project 1/Data/lastData/lastTrain.arff");
//            Instances trainData = train.getDataSet();
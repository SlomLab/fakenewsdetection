package com.example.sulimanhassan.fakenewsdetection;

import android.renderscript.ScriptGroup;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static android.content.ContentValues.TAG;

public class CrowdSourcingManager {


    private final ArrayList<User> users = new ArrayList<User>();

    private final static int MIN = 3;
    private final static int MAX = 10;

    private DatabaseReference DomainsRef, UsersRef, NewsRef;
    private FirebaseDatabase database;
    private FirebaseUser currentUser;
    public final ArrayList<Boolean> FinishReading = new ArrayList<Boolean>();
    private final int MAX_LOAD = 5;
    final int[] i = {0};
    private String[] Ints = MainActivity.INTERESTS;
    static CountDownLatch latch = new CountDownLatch(3);


    public CrowdSourcingManager() {

        database = FirebaseDatabase.getInstance();
        DomainsRef = database.getReference("Domains");
        UsersRef = database.getReference("Users");
        NewsRef = database.getReference("News");
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        FinishReading.add(false);


    }

    public void selectUsers(String newsId, final String Int) {

        fetchUsers(newsId, Int);
    }

    private void fetchUsers(final String newsId, final String Int) {

        if (visited == null) {
            visited = Int;
        }

        DatabaseReference DomainsRef =
                FirebaseDatabase.getInstance().getReference("Domains");
        UsersRef = database.getReference("Users");
        NewsRef = database.getReference("News");
        DomainsRef.child(Int).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() == null) {
                    cont(newsId, Int);
                }

                final long[] size = {0};

                for (final DataSnapshot ds : dataSnapshot.getChildren()) {
                    size[0] = dataSnapshot.getChildrenCount();
                    UsersRef.child(ds.getKey()).child("info").addListenerForSingleValueEvent(
                            new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (users.size() >= MIN && users.size() <= MAX) {
                                        cont(newsId, Int);
                                    } else {
                                        users.add(dataSnapshot.getValue(User.class));
                                        users.get(i[0]).setKey(ds.getKey());
                                        users.get(i[0]).setKey(ds.getKey());

                                        i[0] += 1;

                                        if (size[0] == i[0]) {
                                            cont(newsId, Int);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            }
                    );
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });


    }

    String visited = null;

    private void cont(String newsId, String Int) {
        final MainActivity mainActivity = new MainActivity();

        for (int i = users.size() - 1; i >= 0; i--) {
            if (users.get(i).getLode() >= 5) {
                users.remove(i);
            }
        }
        i[0] = users.size();

        if (users.size() >= MIN) {
            P(Int);
            sendToVoters(newsId);
            saveVoters(newsId);
        } else {
            int place = 0;
            for (int i = 0; i < Ints.length; i++) {
                if (Ints[i].equals(Int)) {
                    place = i;
                }
            }
            for (int j = place; j < Ints.length; j++) {

                if (j == Ints.length - 1 && users.size() > 0) {
                    P(Int);
                    sendToVoters(newsId);
                    saveVoters(newsId);
                }
                fetchUsers(newsId, j + 1 == Ints.length ? Ints[0] : Ints[j + 1]);

            }
        }


    }

    private void P(String Int) {

        for (int i = 0; i < users.size(); i++) {
            double cp = (MAX_LOAD / (1 + users.get(i).getLode())) + Math.abs(users.get(i).getWeight());
            users.get(i).setP(cp);
//            Log.d(TAG, "P: " + users.get(i).getDomain());
            if (users.get(i).getDomain().equals(Int)) {
                users.get(i).setP(cp + 100);
            }
        }
//        Log.d(TAG, "1P: " + users.size());
        for (int i = 0; i < users.size(); i++) {

            int temp = i;
            for (int j = 1; j < users.size(); j++) {
                if (users.get(temp).getP() < users.get(j).getP())
                    temp = j;
            }
            Collections.swap(users, i, temp);
        }
//        Log.d(TAG, "2P: " + users.size());

    }

    private void sendToVoters(String newsId) {
        for (int i = 0; i < users.size(); i++) {
//            Log.d(TAG, "sendToVoters: " + users.get(i).getKey() + " :: " + newsId);
            UsersRef.child(users.get(i).getKey()).child("To Vote").child(newsId).setValue("");


            // update the load.
            UsersRef.child(users.get(i).getKey()).child("info")
                    .child("lode").setValue(users.get(i).getLode() + 1);
        }

    }

    private void saveVoters(String newsId) {
        for (int i = 0; i < users.size(); i++) {
            NewsRef.child(newsId).child("Selected Voters").child(users.get(i).getKey()).setValue("");
        }
        NewsRef.child(newsId).child("Selected Voters").child("size").setValue(users.size());
        NewsRef.child(newsId).child("Selected Voters").child("voted").setValue(0);
//        RequestFragment.changeStatus();
        RequestFragment.progressBar2.setVisibility(View.GONE);
    }


    public void finalDecision(String newsId) {
        NewsRef.child(newsId).child("Selected Voters").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

}

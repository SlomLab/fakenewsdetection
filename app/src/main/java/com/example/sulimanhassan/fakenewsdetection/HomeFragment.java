package com.example.sulimanhassan.fakenewsdetection;


import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private FirebaseUser currentUser;
    private DatabaseReference myRef;
    private FirebaseDatabase database;
    private ArrayList<News2> news2;
    private NewsAdapter AA;
    private ListView newsList;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("TimeLine");
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        news2 = new ArrayList<News2>();

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("News");
        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
//                    Log.d(TAG, "inttt: "+ds.getKey()+":::"+ds.getValue());
//                    News2 cnews = new News2(ds.child("url").getValue().toString(), ds.child("requester").getValue().toString(), ds.child("interest").getValue().toString(), ds.child("news").getValue().toString()
//                            , ds.child("key").getValue().toString(), ds.child("crowdDecision").getValue(Double.class),ds.child("classifierDecision").getValue(Double.class),
//                            ds.child("adimnDecision").getValue(Integer.class));
                    News2 cnews = ds.getValue(News2.class);
                    cnews.setCrowdDecision(ds.child("crowdDecision").getValue(Double.class));
                    cnews.setClassifierDecision(ds.child("classifierDecision").getValue(Integer.class));

                    if (cnews.getClassifierDecision() != 0 || cnews.getCrowdDecision() != 0
                            || cnews.getAdimnDecistion() != 0) {
                        news2.add(cnews);

                    }

                }
                Collections.sort(news2);
                newsList.invalidateViews();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        AA = new NewsAdapter(getActivity(), news2);
        newsList = (ListView) v.findViewById(R.id.list);
        newsList.setAdapter(AA);

        return v;
    }

}

package com.example.sulimanhassan.fakenewsdetection;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import android.widget.FrameLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;


public class MainActivity extends AppCompatActivity {

    private BottomNavigationView mMainNav;
    private FrameLayout mMainFrame;
    public static User u;
    private AccountFragment accountFragment;
    private HomeFragment homeFragment;
    private RequestFragment requestFragment;
    private VoteFragment voteFragment;

    private DatabaseReference UsersRef, NewsRef;
    private FirebaseDatabase database;
    private FirebaseUser currentUser;
    private final long ONE_DAY = 86400000;
    private int counter;
    public static final String[] INTERESTS = {"National", "International", "Local", "Regional", "Business and Fin", "Ents and Celeb",
            "Health and Education", "Arts and Culture", "Sports news", "Politics", "Science and Tech"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        counter = 9;
        setContentView(R.layout.activity_main);
        Log.d("asdasd", "onCreate: ");

        database = FirebaseDatabase.getInstance();
        UsersRef = database.getReference("Users");
        NewsRef = database.getReference("News");
        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        u = new User();
        mMainNav = (BottomNavigationView) findViewById(R.id.main_nav);
        mMainFrame = (FrameLayout) findViewById(R.id.main_frame);
        accountFragment = new AccountFragment();
        homeFragment = new HomeFragment();
        requestFragment = new RequestFragment();
        voteFragment = new VoteFragment();
        int source = 1;

        if (getIntent().getExtras() == null) {
            setFragment(homeFragment, "1");
        } else {
            mMainNav.setSelectedItemId(R.id.nav_account);
            setFragment(accountFragment, "3");
        }

        mMainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.nav_home:
                        setFragment(homeFragment, "1");
                        return true;

                    case R.id.nav_req:
                        setFragment(requestFragment, "2");
                        return true;

                    case R.id.nav_account:
                        setFragment(accountFragment, "3");
                        return true;

                    case R.id.nav_vote:
                        setFragment(voteFragment, "4");
                        return true;

                    default:
                        return false;
                }
            }
        });
        mMainNav.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem menuItem) {

            }
        });

    }

    private void setFragment(Fragment fragment, String tag) {
        if (counter++ >= 9) {
            checkIfTimeForVotingFinish("To Vote");
            checkIfTimeForVotingFinish("Voted");
            checkLoadTimestampAndRefresh();
            counter = 0;
        }
        Log.d(getCallingPackage(), "setFragment() returned: " + fragment.toString());
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment, tag);
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();
    }

    public void checkIfTimeForVotingFinish(String child) {
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            UsersRef.child(currentUser.getUid()).child(child).addValueEventListener(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (final DataSnapshot ds : dataSnapshot.getChildren()) {
                                NewsRef.child(ds.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        try {
                                            News2 n = dataSnapshot.getValue(News2.class);
                                            n.setCrowdDecision(dataSnapshot.child("crowdDecision").getValue(Double.class));
                                            n.setClassifierDecision(dataSnapshot.child("classifierDecision").getValue(Integer.class));
                                            final Double decisions[] = {0.0};
                                            final Double weights[] = {0.0};

                                            if (n.getCrowdDecision() == 0 && n.getAdimnDecistion() == 0) {

                                                if (System.currentTimeMillis() - n.getTimestamp() >= ONE_DAY ||
                                                        ((Long) dataSnapshot.child("Selected Voters").child("voted").getValue())
                                                                == (Long) dataSnapshot.child("Selected Voters").child("size").getValue()) {

                                                    for (DataSnapshot ds : dataSnapshot.child("Selected Voters").getChildren()) {
                                                        decisions[0] += (ds.child("Decision").getValue() == null ? 0.0 : ds.child("Decision").getValue(Double.class));
                                                        weights[0] += (ds.child("Weight").getValue() == null ? 0.0 : ds.child("Weight").getValue(Double.class));
                                                    }

                                                    weights[0] = weights[0] == 0.0 ? 0.0 : Math.abs(weights[0]);
                                                    NewsRef.child(ds.getKey()).child("crowdDecision").setValue(weights[0]);
                                                    UsersRef.child(currentUser.getUid()).child("Voted").child(ds.getKey()).setValue("");
                                                    UsersRef.child(currentUser.getUid()).child("To Vote").child(ds.getKey()).setValue(null);
                                                }
                                            } else {
                                                UsersRef.child(currentUser.getUid()).child("Voted").child(ds.getKey()).setValue("");
                                                UsersRef.child(currentUser.getUid()).child("To Vote").child(ds.getKey()).setValue(null);
                                            }
                                        } catch (Exception e) {

                                        }
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) { } });  } }
                        @Override
                        public void onCancelled(DatabaseError databaseError) { }}); }
    }

    public void checkLoadTimestampAndRefresh() {

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        final DatabaseReference us = FirebaseDatabase.getInstance().getReference("Users");

        us.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    long Loadtimestamp = 0;

                    if (ds.child("info").child("Loadtimestamp").getValue(Long.class) != null) {
                        while (Loadtimestamp == 0) {
                            Loadtimestamp = ds.child("info").child("Loadtimestamp").getValue(Long.class);
                            if (Loadtimestamp > 0 && System.currentTimeMillis() - Loadtimestamp >= ONE_DAY) {
                                Log.d("sjdfa", "-----------------------------------------2------: ");
                                us.child(ds.getKey()).child("info").child("lode").setValue(0);
                                us.child(ds.getKey()).child("info").child("Loadtimestamp").setValue(System.currentTimeMillis());
                                Log.d("sjdfa", "-----------------------------------------2------: ");
                            }
                        }
                    } else {
                        Log.d("sjdfa", "-----------------------------------------------: ");
                        us.child(ds.getKey()).child("info").child("Loadtimestamp").setValue(System.currentTimeMillis());
                        Log.d("sjdfa", "-----------------------------------------------: ");
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

}

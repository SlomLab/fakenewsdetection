package com.example.sulimanhassan.fakenewsdetection;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;

import static android.content.ContentValues.TAG;

public class News {


    private String news;
    private String key;
    private int decision;


    public News(String news, String key) {
        this.news = news;
        this.key = key;
        decision = -9;
    }

    public int getDecision() {
        return decision;
    }

    public void setDecision(int decision) {
        this.decision = decision;
    }

    public String getNews() {
        return news;
    }

    public void setNews(String news) {
        this.news = news;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void fetchNewsFromUrl(final String url)  {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final String u = url;
//                        "https://twitter.com/FoodRhythms/status/461201880354271232/photo/1";
                Document doc = null;
                try {
                    doc = Jsoup.connect(u).get();
                    Element tweetText = doc.select("p.js-tweet-text.tweet-text").first();
                    Log.d(TAG, "fetchNewsFromUrl: "+tweetText.text());
                    setNews(tweetText.text());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();


    }


}

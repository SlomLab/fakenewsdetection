package com.example.sulimanhassan.fakenewsdetection;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;

import static android.content.ContentValues.TAG;

public class News2 implements Comparable {

    private double accuracy;
    private int adimnDecision;
    private int classifierDecision;
    private double crowdDecision;
    private int decision = -9;
    private String interest;
    private String key;
    private String news;
    private String requester;
    private long timestamp;
    private String URL;

    private FirebaseUser currentUser;
    private DatabaseReference myRef;
    private FirebaseDatabase database;


    public News2() {
        decision = -9;
    }

    public News2(News2 value) {

        classifierDecision = 0;
        accuracy = 0;
        crowdDecision = value.getCrowdDecision();
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }


    public News2(String url, String ID, String Int, String news, String key, double crowdDecision, int classifierDecision,
                 int adimnDecision) {
        URL = url;
        requester = ID;
        interest = Int;
        this.news = news;
        this.key = key;
        this.classifierDecision = classifierDecision;
        accuracy = 0;
        this.crowdDecision = crowdDecision;
        this.adimnDecision = adimnDecision;
        decision = -9;
    }


    public News2(String url, String ID, String Int, String news, String key) {
        URL = url;
        requester = ID;
        interest = Int;
        this.news = news;
        this.key = key;
        classifierDecision = 0;
        accuracy = 0;
        crowdDecision = 0;
        adimnDecision = 0;
        decision = -9;
    }

    public News2(String news, boolean finalD, String by) {
        this.news = news;
        this.key = "";
        classifierDecision = 0;
        accuracy = 0;
        crowdDecision = 0;
        adimnDecision = 0;
        decision = -9;
    }

    public News2(String news, boolean finalD, String by, String key) {
        this.news = news;

        this.key = key;
        classifierDecision = 0;
        accuracy = 0;
        crowdDecision = 0;
        adimnDecision = 0;
        decision = -9;
    }

    public String getNews() {
        return news;
    }


    public void setNews(String news) {
        this.news = news;
    }

    public int getClassifierDecision() {
        return classifierDecision;
    }

    public void setClassifierDecision(int classifierDecision) {
        this.classifierDecision = classifierDecision;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    public double getCrowdDecision() {
        return crowdDecision;
    }

    public void setCrowdDecision(double crowdDecision) {
        this.crowdDecision = crowdDecision;
    }

    public int getAdimnDecistion() {
        return adimnDecision;
    }

    public void setAdimnDecistion(int adimnDecistion) {
        this.adimnDecision = adimnDecistion;
    }

    public String getRequester() {
        return requester;
    }

    public void setRequester(String requester) {
        this.requester = requester;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }


    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void fetchNewsFromUrl(final String url) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final String u = url;
//                        "https://twitter.com/FoodRhythms/status/461201880354271232/photo/1";
                Document doc = null;
                try {
                    doc = Jsoup.connect(u).get();
                    Element tweetText = doc.select("p.js-tweet-text.tweet-text").first();
                    setNews(tweetText.text());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public int getDecision() {
        return decision;
    }

    public double getFinalDecision(){
        if (getAdimnDecistion() != 0){
            return getAdimnDecistion();
        }
        if (getCrowdDecision() != 0){
            return getCrowdDecision();
        }
        if (getClassifierDecision() != 0){
            return getClassifierDecision();
        }
        return -9;
    }

    public void setDecision(int decision) {
        this.decision = decision;
    }


    public void Req(String url, String ID, final String Int, final String key, Context context) {
        setRequester(ID);
        setURL(url);
        setInterest(Int);
        setKey(key);
        try {
            final boolean[] toast = {false};
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {

                    database = FirebaseDatabase.getInstance();
                    myRef = database.getReference("News");
                    currentUser = FirebaseAuth.getInstance().getCurrentUser();
                    Document doc = null;
                    Element tweetText = null;
                    try {
                        doc = Jsoup.connect(getURL()).get();
                        tweetText = doc.select("p.js-tweet-text.tweet-text").first();
                        setNews(tweetText.text() == null ? "" : tweetText.text());
                        News2 n = new News2(getURL(), getRequester(), getInterest(), getNews(), getKey());

                        n.setKey(getKey());
                        n.setTimestamp(System.currentTimeMillis());
                        myRef.child(getKey()).setValue(n);
//                    myRef.child(String.valueOf(ref))
                        myRef.getRoot().child("Users").child(currentUser.getUid()).child("Reqs").child(getKey()).setValue("");

                        assessAuthienticity(tweetText.text(), Int, key, context);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
            thread.join();
            RequestFragment.progressBar2.setVisibility(View.GONE);
        } catch (Exception e){
            e.getMessage();
        }
    }

    private void assessAuthienticity(String text, final String Int, final String key, Context context) {
        final CrowdSourcingManager[] crowdSourcingManager = new CrowdSourcingManager[1];
        int result = Classifier.PredictInstance(text, context);
        if (result != 0) {
            myRef = database.getReference("News");
            myRef.child(key).child("classifierDecision").setValue(result);
        } else {
            crowdSourcingManager[0] = new CrowdSourcingManager();
            crowdSourcingManager[0].selectUsers(key, Int);
        }
    }


    @Override
    public int compareTo(@NonNull Object o) {


        Long comapreKey = Long.parseLong((((News2) o).getKey()));

        Long x = Long.parseLong(this.getKey());
        int xx = x.intValue();
        Log.d(TAG, "compareTo: " + comapreKey.intValue() + " <> "+ xx);
        return comapreKey.intValue() - xx;
    }
}

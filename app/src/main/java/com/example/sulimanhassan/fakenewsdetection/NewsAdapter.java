package com.example.sulimanhassan.fakenewsdetection;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class NewsAdapter extends ArrayAdapter<News2> {



    public NewsAdapter(Activity context, ArrayList<News2> news2) {
        super(context, 0, news2);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View listItemView = convertView;
        if(listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.news, parent, false);
        }

        News2 currentNews = getItem(position);

        TextView tTextView = (TextView) listItemView.findViewById(R.id.news_text);
        tTextView.setText(currentNews.getNews());

        TextView fTextView = (TextView) listItemView.findViewById(R.id.finald);
        TextView dTextView = (TextView) listItemView.findViewById(R.id.who_class);

        Log.d(TAG, "getView: admin: "+ currentNews.getAdimnDecistion()+ " "+(currentNews.getAdimnDecistion() != 0) );
        if (currentNews.getAdimnDecistion() != 0){
            Log.d(TAG, "truee: ");
            fTextView.setText(currentNews.getAdimnDecistion() == 1 ? "Fake" : "Not Fake");
            dTextView.setText("By Admin");
        } else if (currentNews.getCrowdDecision() != 0 ){
            fTextView.setText(currentNews.getCrowdDecision() == 1 ? "Fake" : "Not Fake");
            dTextView.setText("By Crowd");
        }else if (currentNews.getClassifierDecision() != 0 ) {
            fTextView.setText(currentNews.getClassifierDecision() == 1 ? "Fake" : "Not Fake");
            dTextView.setText("By System");
        } else {
            fTextView.setText("Unrated");
            dTextView.setText("Waiting ...");
        }

        return listItemView;
    }
}

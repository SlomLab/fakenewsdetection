package com.example.sulimanhassan.fakenewsdetection;


import android.content.Context;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import weka.classifiers.meta.FilteredClassifier;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class RequestFragment extends Fragment {

    private FirebaseUser u1;
    private ArrayList<News2> news2;

    private Spinner sINT;
    public static ProgressBar progressBar2;
    private EditText eURL;
    private Button bREQ;
    private NewsAdapter AA;
    private DatabaseReference myRef, newsRef;
    private FirebaseDatabase database;
    private FirebaseUser currentUser, checkUser;
    private String URL;
    private String Int;
    private ListView newsList;

    public RequestFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkUser = FirebaseAuth.getInstance().getCurrentUser();
        news2 = new ArrayList<News2>();
//        getRequestedNews();
        Log.d(TAG, "onCreate: ");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        getActivity().setTitle("Req");
        View v;
        database = FirebaseDatabase.getInstance();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();


        if (currentUser == null) {
            v = inflater.inflate(R.layout.dummy_layout, container, false);
            getActivity().setTitle("Please Sign In");
            TextView signBtn = (TextView) v.findViewById(R.id.signBtn);
            signBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((BottomNavigationView) getActivity().findViewById(R.id.main_nav)).setSelectedItemId(R.id.nav_account);
                    AccountFragment accountFragment = new AccountFragment();
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.main_frame, accountFragment, "findThisFragment")
                            .addToBackStack(null)
                            .commit();
                }
            });

        } else {
            v = inflater.inflate(R.layout.fragment_request, container, false);

            progressBar2 = v.findViewById(R.id.progressBar2);
            progressBar2.setVisibility(View.GONE);
            newsList = (ListView) v.findViewById(R.id.list1);
            sINT = (Spinner) v.findViewById(R.id.intSpinner);
            myRef = database.getReference("Users").child(currentUser.getUid()).child("Reqs");
            newsRef = database.getReference("News");
            eURL = (EditText) v.findViewById(R.id.tURL);
            bREQ = (Button) v.findViewById(R.id.bREQ);
            final News2 news2 = new News2();

//            Classifier classifier = new Classifier();
//            FilteredClassifier naiveBayes=null;
//            AssetManager assetManager = getActivity().getAssets();
//            try {
//                 naiveBayes = (FilteredClassifier) weka.core.SerializationHelper.read(assetManager.open("naiveBayes.model"));
//                 Log.d(TAG, "assessAuthienticity: "+ classifier.PredictInstance("heloo im suliman man",naiveBayes));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }


            bREQ.setOnClickListener(new View.OnClickListener() {


                @Override
                public void onClick(View view) {
                    final CrowdSourcingManager[] crowdSourcingManager = new CrowdSourcingManager[1];
                    RequestFragment.progressBar2.setVisibility(View.VISIBLE);
                    Thread thread = new Thread();

                    thread = new Thread(new Runnable() {

                        @Override
                        public void run() {

                            URL = eURL.getText().toString();
                            eURL.setText("");
                            Int = sINT.getSelectedItem().toString();
                            String key = String.valueOf(System.currentTimeMillis());
                            news2.Req(URL, currentUser.getUid().toString(), Int, key, getContext());

                        }
                    });
                    thread.start();

                }
            });

            // get news and listen for any addition
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (final DataSnapshot id : dataSnapshot.getChildren()) {
                        newsRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot newsId : dataSnapshot.getChildren()) {
                                    if (newsId.getKey().equals(id.getKey())) {
                                        News2 cnews = newsId.getValue(News2.class);
                                        if (checkIfNewsExist(newsId.getKey().toString()) == false) {
                                            RequestFragment.this.news2.add(cnews);
                                            Log.d(TAG, "onDataChange: " + cnews.getKey());
//                                            for (News2 n : RequestFragment.this.news2
    //                                                    ) {
//                                                Log.d(TAG, "beforeSort: " + n.getKey());
//
//                                            }
//                                            Collections.sort(RequestFragment.this.news2);
                                            AA.notifyDataSetChanged();
//
//                                            for (News2 n : RequestFragment.this.news2
//                                                    ) {
//                                                Log.d(TAG, "afterSort: " + n.getKey());
//
//                                            }

                                        }
                                    }
                                }
//                                int size = RequestFragment.this.news2.size();
//                                for (int i = 0; i < size / 2; i++) {
//                                    final News2 n = RequestFragment.this.news2.get(i);
//                                    RequestFragment.this.news2.set(i, RequestFragment.this.news2.get(size - i - 1)); // swap
//                                    RequestFragment.this.news2.set(size - i - 1, n); // swap
//                                }
//                                AA.notifyDataSetChanged();
//                                for (News2 n : RequestFragment.this.news2
//                                        ) {
//                                    Log.d(TAG, "endSort: " + n.getKey());
//
//                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });

            // check if a news have been deleted.
            listenForChanging();

            AA = new NewsAdapter(getActivity(), this.news2);
            newsList = (ListView) v.findViewById(R.id.list1);
            newsList.setAdapter(AA);
        }

        return v;
    }

    private void listenForChanging() {

        newsRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                for (int i = 0; i < news2.size(); i++) {
                    if (news2.get(i).getKey().equals(dataSnapshot.getKey())) {
                        news2.remove(i);
                        news2.add(dataSnapshot.getValue(News2.class));
//                        Collections.sort(news2);
                        AA.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public static void changeStatus() {
        RequestFragment.progressBar2.setVisibility(View.GONE);
    }


//    public void getRequestedNews() {
//
//
//        database = FirebaseDatabase.getInstance();
//        currentUser = FirebaseAuth.getInstance().getCurrentUser();
//        u1 = currentUser;
//        if (currentUser != null) {
//            myRef = database.getReference("Users").child(currentUser.getUid()).child("Reqs");
//            newsRef = database.getReference("News");
////            news2 = new ArrayList<News2>();
//            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//                    for (final DataSnapshot id : dataSnapshot.getChildren()) {
//                        newsRef.addListenerForSingleValueEvent(new ValueEventListener() {
//                            @Override
//                            public void onDataChange(DataSnapshot dataSnapshot) {
//                                for (DataSnapshot newsId : dataSnapshot.getChildren()) {
//                                    if (newsId.getKey().equals(id.getKey())) {
//                                        News2 cnews = newsId.getValue(News2.class);
//                                        Log.d(TAG, "inner lp " + cnews.getCrowdDecision());
//                                        if (checkIfNewsExist(newsId.getKey().toString()) == false) {
//                                            news2.add(cnews);
//
//                                        }
//
//
//                                    }
//                                }
//
////                                newsList.invalidateViews();
//                                Collections.sort(news2);
//                                AA = new NewsAdapter(getActivity(), news2);
//                                newsList.setAdapter(AA);
//                                Log.d(TAG, "news size: " + news2.size());
//
//                            }
//
//
//                            @Override
//                            public void onCancelled(DatabaseError databaseError) {
//
//                            }
//                        });
//                    }
//
//                    newsList.invalidateViews();
//                    Log.d(TAG, "news size: " + news2.size());
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//                }
//
//            });
//
//            Log.d(TAG, "onCreate: ");
//            Log.d(TAG, "news size: " + news2.size());
//
//            AA = new NewsAdapter(getActivity(), news2);
//
//
//        }
//
//    }

    public boolean checkIfNewsExist(String key) {

        if (news2 != null && news2.size() > 0)
            for (int i = 0; i < news2.size(); i++) {
                if (news2.get(i).getKey() != (null) && news2.get(i).getKey().equals(key)) {
                    return true;
                }
            }
        return false;
    }

}

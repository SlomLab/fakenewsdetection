package com.example.sulimanhassan.fakenewsdetection;

import android.support.annotation.NonNull;

public class User {

    private String gender;
    private String location;
    private int weight = 0;
    private String age;
    private String domain;
    private String key;
    private boolean sameInt;
    private double P;
    private int lode;
    private long Loadtimestamp = System.currentTimeMillis();


    public User() {

        gender = "";
        location = "";
        age = "";
    }

    public User(String gender, String location, String domain, String age, String key) {
        this.gender = gender;
        this.location = location;
        this.age = age;
        this.domain = domain;
        this.key = key;
    }

//    public User(String gender, String location, String experience, String age, String domain) {
//
//        this.gender = gender;
//        this.location = location;
//        this.age = age;
//        this.domain = domain;
//    }

    public int getLode() {
        return lode;
    }

    public void setLode(int lode) {
        this.lode = lode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }


    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isSameInt() {
        return sameInt;
    }

    public void setSameInt(boolean sameInt) {
        this.sameInt = sameInt;
    }

    public double getP() {
        return P;
    }

    public void setP(double p) {
        P = p;
    }

    public long getLoadtimestamp() {
        return Loadtimestamp;
    }

    public void setLoadtimestamp(long loadtimestamp) {
        Loadtimestamp = loadtimestamp;
    }



}

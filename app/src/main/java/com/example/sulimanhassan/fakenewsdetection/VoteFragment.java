package com.example.sulimanhassan.fakenewsdetection;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class VoteFragment extends Fragment {

    private ArrayList<News2> news;
    private ListView newsListView;
    private SeekBar rateBar;
    private SeekBar[] sb;
    private TextView newsTextView;
    private Button voteBtn;
    private customAdapter ca;
    private final int decision[] = {1, -1, 1};
    private FirebaseUser currentUser;
    private BottomNavigationView mMainNav;
    private DatabaseReference UsersRef, NewsRef;
    private FirebaseDatabase database;


    public VoteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        news = new ArrayList<News2>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;

        database = FirebaseDatabase.getInstance();
        UsersRef = database.getReference("Users");
        NewsRef = database.getReference("News");
        currentUser = FirebaseAuth.getInstance().getCurrentUser();


        // check if the user already logged in or not
        if (currentUser == null) {
            view = inflater.inflate(R.layout.dummy_layout, container, false);
            getActivity().setTitle("Please Sign In");
            TextView signBtn = (TextView) view.findViewById(R.id.signBtn);
            signBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((BottomNavigationView) getActivity().findViewById(R.id.main_nav)).setSelectedItemId(R.id.nav_account);
                    AccountFragment accountFragment = new AccountFragment();
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.main_frame, accountFragment, "findThisFragment")
                            .addToBackStack(null)
                            .commit();
                }
            });


        } else {
            view = inflater.inflate(R.layout.fragment_vote, container, false);
            getActivity().setTitle("Votes");

            voteBtn = (Button) view.findViewById(R.id.voteBtn);
            newsListView = (ListView) view.findViewById(R.id.newsListView);
            voteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    uploadVote();
                }
            });
            newsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    sb[i].setEnabled(!sb[i].isEnabled());
                }
            });

            Log.d(TAG, "onCreateView: " + currentUser.getUid());
            UsersRef.child(currentUser.getUid().toString()).child("To Vote").addValueEventListener(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Log.d(TAG, "onDataChange: " + dataSnapshot.getKey().toString());
                            for (DataSnapshot ds : dataSnapshot.getChildren()) {
                                Log.d(TAG, "onDataChange: ");
                                NewsRef.child(ds.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        News2 n = dataSnapshot.getValue(News2.class);
                                        Log.d(TAG, "onDataChange: " + n);
                                        if (!checkIfNewsExist(n.getKey())) {
                                            Log.d(TAG, "onDataChange: " + n.getNews());
                                            news.add(dataSnapshot.getValue(News2.class));
                                            Collections.sort(news);
                                            sb = new SeekBar[news.size()];
                                            ca.notifyDataSetChanged();
//                                            ca = new customAdapter();
//                                            newsListView.setAdapter(ca);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    }
            );

            sb = new SeekBar[news.size()];
            ca = new customAdapter();
            newsListView.setAdapter(ca);

//            n.fetchNewsFromUrl("https://twitter.com/FoodRhythms/status/461201880354271232/photo/1");
        }
        return view;

    }

    private void moveNode(final DatabaseReference fromPath, final DatabaseReference toPath) {
        fromPath.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                toPath.child(dataSnapshot.getKey()).setValue(dataSnapshot.getValue(), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError firebaseError, DatabaseReference firebase) {
                        if (firebaseError != null) {
                            Log.d(TAG, "onComplete:Copy failed");
                        } else {
                            Log.d(TAG, "onComplete:Copy success");
                        }
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public boolean checkIfNewsExist(String key) {

        if (news != null && news.size() > 0)
            for (int i = 0; i < news.size(); i++) {
                if (news.get(i).getKey() != (null) && news.get(i).getKey().equals(key)) {
                    return true;
                }
            }
        return false;
    }

    private void uploadVote() {
        try {
            for (int i = 0; i < news.size(); i++) {
                if (sb[i].isEnabled()) {
                    final int[] finalI = {i};
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            FirebaseDatabase.getInstance().getReference("Users").child(currentUser.getUid()).child("Voted").child(news.get(finalI[0]).getKey()).setValue("");
                            FirebaseDatabase.getInstance().getReference("Users").child(currentUser.getUid()).child("To Vote").child(news.get(finalI[0]).getKey()).setValue(null);

                            // save the vote value into the news ...
                            final double[] weightForCurrentUser = {0.0};
                            FirebaseDatabase.getInstance().getReference("Users").child(currentUser.getUid()).child("info").child("weight").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    weightForCurrentUser[0] =  dataSnapshot.getValue(Double.class);
                                    FirebaseDatabase.getInstance().getReference("News").child(news.get(finalI[0]).getKey()).child("Selected Voters").child(currentUser.getUid())
                                            .child("Decision").setValue((sb[finalI[0]].getProgress() - 1) * weightForCurrentUser[0]);

                                    FirebaseDatabase.getInstance().getReference("News").child(news.get(finalI[0]).getKey()).child("Selected Voters").child(currentUser.getUid())
                                            .child("Weight").setValue(weightForCurrentUser[0]);

                                    FirebaseDatabase.getInstance().getReference("News").child(news.get(finalI[0]).getKey()).child("Selected Voters")
                                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    FirebaseDatabase.getInstance().getReference("News").child(news.get(finalI[0]).getKey()).
                                                            child("Selected Voters").child("voted")
                                                            .setValue((Long) dataSnapshot.child("voted").getValue() + 1);
                                                    news.get(finalI[0]).setDecision(sb[finalI[0]].getProgress() - 1);
                                                    news.remove(finalI[0]--);
                                                    ca.notifyDataSetChanged();
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            });
                                    ca.notifyDataSetChanged();

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });


                        }
                    }).start();
                    // move the news from To Vote to Voted ...

                }
            }


        } catch (Exception e) {
            Log.d(TAG, "uploadVote: " + e.getMessage());
        }


    }

    private void refreshList() {
        for (int i = news.size() - 1; i >= 0; i--) {
            if (news.get(i).getDecision() != -9)
                news.remove(i);
        }
        ca.notifyDataSetChanged();
    }

    class customAdapter extends BaseAdapter {


        customAdapter() {
            sb = new SeekBar[news.size()];

        }

        @Override
        public int getCount() {
            return news.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }


        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            View v = getLayoutInflater().inflate(R.layout.custom_layout, null);
            newsTextView = (TextView) v.findViewById(R.id.newsTextView);
            newsTextView.setText(news.get(i).getNews());
            rateBar = (SeekBar) v.findViewById(R.id.rateBar);


            sb[i] = rateBar;
            sb[i].setEnabled(false);
//
//            rateBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//                @Override
//                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
//                }
//
//                @Override
//                public void onStartTrackingTouch(SeekBar seekBar) {
//
//                }
//
//                @Override
//                public void onStopTrackingTouch(SeekBar seekBar) {
//
//                }
//
//            });

            return v;
        }

    }

}

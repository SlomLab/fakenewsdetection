package com.example.sulimanhassan.fakenewsdetection;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class signUpFragment extends Fragment {

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    //    private DatabaseReference myRef = database.getReference("message");
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private EditText mEmail;
    private EditText mPassword1;
    private EditText mPassword2;
    private Button mNextBtn;
    private ProgressBar progressBar;
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id = view.getId();
            switch (id) {
                case R.id.NextFirstBtn:
                    SignUpNextPage();
                    break;
                default:
                    break;
            }
        }

    };


    public signUpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("SIGN UP");
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        mEmail = (EditText) view.findViewById(R.id.signUpEmailText);
        mEmail.requestFocus();
        mPassword1 = (EditText) view.findViewById(R.id.passwordFirstText);
        mPassword2 = (EditText) view.findViewById(R.id.passwordSecText);


        mNextBtn = (Button) view.findViewById(R.id.NextFirstBtn);
        mNextBtn.setOnClickListener(mOnClickListener);


        return view;
    }

    private void SignUpNextPage() {
        String email = "";
        String password = "";
        try {
            email = mEmail.getText().toString();
            if (mPassword1.getText().toString().equals(mPassword2.getText().toString())) {
                password = mPassword1.getText().toString();

                progressBar.setVisibility(View.VISIBLE);
                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener((Activity) getContext(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    progressBar.setVisibility(View.GONE);
                                    // Sign in success, update UI with the signed-in user's information
                                    Log.d(TAG, "createUserWithEmail:success");
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    signUpFragment2 signUpFragment2 = new signUpFragment2();
                                    getActivity().getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.main_frame, signUpFragment2)
                                            .addToBackStack(null)
                                            .commit();

                                } else {
                                    progressBar.setVisibility(View.GONE);
                                    // If sign in fails, display a message to the user.
                                    Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                    Toast.makeText(getActivity().getApplicationContext(), "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();

                                }

                                // ...
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity().getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
            else {
                Toast.makeText(getActivity().getApplicationContext(), "Passwords does not match", Toast.LENGTH_LONG).show();
                mPassword1.requestFocus();
                mPassword1.selectAll();
            }
        } catch (Exception e) {
            Toast.makeText(getActivity().getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }

    }

}

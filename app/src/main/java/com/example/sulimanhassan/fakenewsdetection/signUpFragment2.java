package com.example.sulimanhassan.fakenewsdetection;


import android.os.Bundle;
import android.os.CpuUsageInfo;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class signUpFragment2 extends Fragment implements AdapterView.OnItemSelectedListener {


    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    //    private DatabaseReference myRef = database.getReference("message");
    private Spinner SexSpinner;
    private Spinner CountrySpinner;
    private Spinner DomainSpinner;
    private EditText AgeEditText;
    private Button NextButton;
    private String country = "";
    private String sex = "";
    private String domain = "";
    private String age = "";
    private FirebaseUser user;
    private DatabaseReference mRef;

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id = view.getId();
            switch (id) {
                case R.id.NextSecBtn:
                    SignUpNextPage();
                    break;
                default:
                    break;
            }
        }

    };


    public signUpFragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up_fragment2, container, false);
        user = mAuth.getCurrentUser();
        Log.d(TAG, mAuth.getUid());
        SexSpinner = (Spinner) view.findViewById(R.id.SexSpinner);
        CountrySpinner = (Spinner) view.findViewById(R.id.CountrySpinner);
        DomainSpinner = (Spinner) view.findViewById(R.id.DomainSpinner);
        AgeEditText = (EditText) view.findViewById(R.id.Age);
        AgeEditText.setText("0");
        AgeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AgeEditText.selectAll();
            }
        });
        NextButton = (Button) view.findViewById(R.id.NextSecBtn);
        mRef = database.getReference("Users");


        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {

                        return true;
                    }
                }
                return false;
            }
        });

        NextButton.setOnClickListener(mOnClickListener);
        this.SexAdapter();
        this.CountriesAdapter();
        this.DomainAdapter();

        // Inflate the layout for this fragment
        return view;
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(adapterView.getContext(), " -->" + adapterView.getId(), Toast.LENGTH_SHORT).show();
        switch (adapterView.getId()) {
            case R.id.SexSpinner:
                sex = adapterView.getItemAtPosition(i).toString();
                Toast.makeText(adapterView.getContext(), "Sex", Toast.LENGTH_SHORT).show();
                break;

            case R.id.CountrySpinner:
                country = adapterView.getItemAtPosition(i).toString();
                Toast.makeText(adapterView.getContext(), "Country", Toast.LENGTH_SHORT).show();
                break;

            case R.id.DomainSpinner:
                domain = adapterView.getItemAtPosition(i).toString();
                Toast.makeText(adapterView.getContext(), view.toString(), Toast.LENGTH_SHORT).show();
                break;

        }
        String text = adapterView.getItemAtPosition(i).toString();
//        Toast.makeText(adapterView.getContext(), view.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void SexAdapter() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity()
                .getApplicationContext(), R.array.sex, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SexSpinner.setAdapter(adapter);
        SexSpinner.setSelection(0);
        SexSpinner.setOnItemSelectedListener(this);
    }

    private void CountriesAdapter() {
        Locale[] locale = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        String country;
        for (Locale loc : locale) {
            country = loc.getDisplayCountry();
            if (country.length() > 0 && !countries.contains(country)) {
                countries.add(country);
            }
        }
        Collections.sort(countries, String.CASE_INSENSITIVE_ORDER);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity()
                .getApplicationContext(), android.R.layout.simple_spinner_item, countries);
        CountrySpinner.setAdapter(adapter);
        CountrySpinner.setSelection(0);
        CountrySpinner.setOnItemSelectedListener(this);
    }

    private void DomainAdapter() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity()
                .getApplicationContext(), R.array.domainArr, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        DomainSpinner.setAdapter(adapter);
        DomainSpinner.setSelection(0);
        DomainSpinner.setOnItemSelectedListener(this);

    }


    private void SignUpNextPage() {
        age = AgeEditText.getText().toString();
        User u = new User(sex, country, domain, age, mAuth.getUid());

        mRef.child(user.getUid()).child("info").setValue(u);
        mRef.getRoot().getDatabase().getReference("Domains").child(domain).child(user.getUid()).setValue("");


        signUpFragment3 signUpFragment3 = new signUpFragment3();

        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_frame, signUpFragment3)
                .disallowAddToBackStack()
                .commit();
    }


}

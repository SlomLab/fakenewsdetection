package com.example.sulimanhassan.fakenewsdetection;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class signUpFragment3 extends Fragment {

    private ListView newsListView;
    private SeekBar rateBar;
    private SeekBar[] sb;
    private TextView newsTextView;
    private ArrayList<News2> news = new ArrayList<News2>();
    private Button FinishBtn;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseUser user = mAuth.getCurrentUser();
    private customAdapter ca;
    private DatabaseReference mRef;

    private final int NUMBER_OF_NEWS = 3;
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id = view.getId();
            switch (id) {
                case R.id.FinishBtn:
                    uploadVote();
                    break;
                default:
                    break;
            }
        }
    };




    public signUpFragment3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up_fragment3, container, false);

        // ---------- refuse the ability to go back in the registering process ----------------
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        return true;
                    }
                }
                return false;
            }
        });
        // --------------------------
        database.getReference().child("News").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int count = 0;
                for (DataSnapshot data : dataSnapshot.getChildren()){
                    News2 n = data.getValue(News2.class);
                    n.setClassifierDecision(data.child("classifierDecision").getValue(Integer.class));
                    n.setAdimnDecistion(data.child("adimnDecistion").getValue(Integer.class));
                    n.setCrowdDecision(data.child("crowdDecision").getValue(Integer.class));

                    if (n.getFinalDecision() != -9 && count < NUMBER_OF_NEWS) {
                        news.add(n);
                        ca.notifyDataSetChanged();
                        sb = new SeekBar[news.size()];
                        count++;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
//        news.add(new News("My name is suliman but my father name is not hassan," +
//                "My name is suliman but my father name is not hassan", "ABMNBJHANBHGNABMGHHA"));
//        news.add(new News("My name is suliman but my father name is not hassan," +
//                "My name is suliman but my father name is not hassan", "IUYHKJKJADHJKAJKSDHK"));
//        news.add(new News("My name is suliman but my father name is not hassan," +
//                "My name is suliman but my father name is not hassan", "QGUAISLAKSJIAUASOSIJD"));
        // --------------------------
        mRef = database.getReference("Users");
        // --------------------------
        newsListView = (ListView) view.findViewById(R.id.newsListView);
        // --------------------------
        FinishBtn = (Button) view.findViewById(R.id.FinishBtn);
        FinishBtn.setEnabled(false);
        newsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                boolean check = true;
                Log.d(TAG, "onItemClick: "+i);
                sb[i].setEnabled(true);
                for (int j = 0; j< NUMBER_OF_NEWS;j++){
                    if (!sb[j].isEnabled()){
                        check = false;
                        break;
                    }
                }
                FinishBtn.setEnabled(check);
            }
        });
        // --------------------------

        FinishBtn.setOnClickListener(mOnClickListener);
        ca = new customAdapter();
        newsListView.setAdapter(ca);
        // --------------------------

        // Inflate the layout for this fragment
        return view;
    }

    private void uploadVote() {
        for (int i = 0; i < NUMBER_OF_NEWS; i++) {
            if (sb[i].isEnabled()) {
                news.get(i).setDecision(sb[i].getProgress()-1);
            }
        }
        FinishVoting();
    }

    private void FinishVoting() {
        // calculate the user accuracy to initialize the initial weight.
        double precision = 0.0;

        for (int i = 0; i< NUMBER_OF_NEWS; i++) {
            Log.d(TAG, "FinishVoting: "+news.get(i).getDecision() +" :::: "+ news.get(i).getFinalDecision()+ " ::: " + news.size());
            precision += news.get(i).getDecision() * news.get(i).getFinalDecision();
        }
        precision /= news.size();

        Log.d(TAG, "FinishVoting: Your Precision is "+ precision);
        mRef.child(user.getUid()).child("info").child("weight").setValue(precision);
        // go to accountDetails fragment
        AccountFragment accountFragment = new AccountFragment();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_frame, accountFragment, "findThisFragment")
                .addToBackStack(null)
                .commit();
    }


    class customAdapter extends BaseAdapter {

        customAdapter(){

            sb = new SeekBar[NUMBER_OF_NEWS];
        }
        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }


        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            View v = getLayoutInflater().inflate(R.layout.custom_layout, null);
            if (news.size() >= 1) {
                newsTextView = (TextView) v.findViewById(R.id.newsTextView);
                newsTextView.setText(news.get(i).getNews());
                rateBar = (SeekBar) v.findViewById(R.id.rateBar);

                sb[i] = rateBar;
                sb[i].setEnabled(false);
            }
            return v;
        }

    }

}
